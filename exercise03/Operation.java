package exercise03;

@FunctionalInterface
public interface Operation {
    // It only contains one method
    public int addition(int num1, int num2);
}
