package exercise03;

public class FuncInterApp {
    public void calculateAddition(int n1, int n2){
        Operation operation = (int num1, int num2) -> num1 + num2;
        System.out.println(operation.addition(n1, n2));
    }

    public static void main(String[] args) {
        FuncInterApp funcInterApp = new FuncInterApp();
        funcInterApp.calculateAddition(5, 7);
    }
}
