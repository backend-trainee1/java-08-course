package exercise10;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class HigherFuncApp {
    private Function<String, String> castUpper = x -> x.toUpperCase();
    private Function<String, String> castLower = x -> x.toLowerCase();

    public void print(Function<String, String> function, String value) {
        System.out.println(function.apply(value));
    }

    public Function<String, String> show(String message) {
        return (String x) -> message + x;
    }

    public void filter(List<String> list, Consumer<String> consumer,
                       int length, String str) {
        list.stream().filter(this.establishLogicalFilter(str)).forEach(consumer);
    }

    public Predicate<String> establishLogicalFilter(int length) {
        return text -> text.length() < length;
    }

    public Predicate<String> establishLogicalFilter(String str) {
        return text -> text.contains(str);
    }

    public static void main(String[] args) {
        HigherFuncApp app = new HigherFuncApp();
        app.print(app.castUpper, "programming");
        app.print(app.castLower, "JAVA");

        String rpta = app.show("Hi ").apply("Dani");
        System.out.println(rpta);

        List<String> list = new ArrayList<>();
        list.add("Math I");
        list.add("Statistics");
        list.add("Operating Systems");

        app.filter(list, System.out::println, 5, null);
    }
}
