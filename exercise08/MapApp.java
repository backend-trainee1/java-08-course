package exercise08;

import exercise07.ParallelStreamApp;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class MapApp {
    private Map<Integer, String> map;

    public void populate() {
        this.map = new HashMap<>();
        map.put(1, "Programming");
        map.put(2, "Maths");
        map.put(3, "Statistics");
    }

    public void print(){
        map.entrySet().stream().forEach(System.out::println);
    }

    public void collect(){
        Map<Integer, String> mapCollect = map.entrySet().stream().
                filter(e->e.getValue().contains("o")).
                collect(Collectors.toMap(p->p.getKey(), p->p.getValue()));

        mapCollect.forEach((key, value)-> System.out.printf("Key: %d - Value: %s", key, value));
    }

    public void putIfAbsent(){
        map.putIfAbsent(2, "Paola");
    }

    public void computeIfPresent(){
        map.computeIfPresent(3, (key, value)->key+value);
        System.out.println(map.get(3));
    }

    public void getOrDefault(){
        String value = map.getOrDefault(5, "Default value");
        System.out.println(value);
    }

    public static void main(String[] args) {
        MapApp app = new MapApp();
        app.populate();
        app.print();
        app.putIfAbsent();
        app.computeIfPresent();
        app.getOrDefault();
        app.collect();

    }
}
