package exercise02;

public class DefaultMethods implements PersonA{
    @Override
    public void walk() {
        System.out.println("This person is walking...");
    }


    public static void main(String[] args) {
        DefaultMethods defaultMethods = new DefaultMethods();
        defaultMethods.walk(); // traditional method
        defaultMethods.talk(); // default method
    }
}


