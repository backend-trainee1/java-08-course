package exercise02;

public interface PersonA {
    public void walk();

    // Default method
    default void talk(){
        System.out.println("Hello");
    }
}
