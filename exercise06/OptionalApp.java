package exercise06;

import java.util.Optional;

public class OptionalApp {

    public void tryFunc(String value){
        try{
            Optional optional = Optional.empty();
            optional.get();
        }catch (Exception e){
            System.out.println("There's no element");
        }
    }

    public void orElse(String value){
        Optional<String> optional = Optional.ofNullable(value);
        String x = optional.orElse("default");
        System.out.println(x);
    }

    public void orElseThrow(String value){
        Optional<String> optional = Optional.ofNullable(value);
        optional.orElseThrow(NumberFormatException::new);
    }

    public void isPresent(String value){
        Optional<String> optional = Optional.ofNullable(value);
        System.out.println(optional.isPresent());
    }

    public static void main(String[] args) {
        OptionalApp app = new OptionalApp();
        app.tryFunc("Dani");
        app.orElse("Dani");
        app.orElseThrow("Dani");
        app.isPresent("Dani");

    }

}
