package exercise01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdaApp {
    public void sortList(){
        List<String> list = new ArrayList<>();
        list.add("Daniela");
        list.add("Carlos");
        list.add("Ari");
        list.add("Emma");

        /*
         * Ordinary way
          Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        */

        // Lambda function
        Collections.sort(list, (String s1, String s2) -> s1.compareTo(s2));

        for(String element : list){
            System.out.println(element);
        }
    }

    public void calculate(){
        /*
        * Ordinary way
        Operations operation = new Operations() {
            @Override
            public double average(double num1, double num2) {
                return (num1 + num2) / 2;
            }
        };
         */

        // Lambda function
        Operations operations = (double num1, double num2) -> (num1 + num2)/2;

        System.out.println(operations.average(2, 3));

    }


    public static void main(String[] args) {
        LambdaApp app = new LambdaApp();
        System.out.println("Sorted names");
        app.sortList();
        System.out.println("\nAverage");
        app.calculate();

    }

}
