package exercise09;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateApp {
    public void verify(int version){
        if (version == 7) {
            Calendar date1 = Calendar.getInstance();
            Calendar date2 = Calendar.getInstance();
            date1.set(2000, 8, 14);
            System.out.println(date1.after(date2));
        }
        else if (version == 8) {
            LocalDate date1 = LocalDate.of(2000, 8, 14);
            LocalDate date2 = LocalDate.now();

            System.out.println(date1.isAfter(date2));
            System.out.println(date1.isBefore(date2));

            LocalTime time1 = LocalTime.of(15, 33, 33);
            LocalTime time2 = LocalTime.now();

            System.out.println(time1.isAfter(time2));
            System.out.println(time1.isBefore(time2));

            LocalDateTime dateTime1 = LocalDateTime
                    .of(2000, 8, 14, 15, 33, 33);
            LocalDateTime dateTime2 = LocalDateTime.now();

            System.out.println(dateTime1.isAfter(dateTime2));
            System.out.println(dateTime1.isBefore(dateTime2));

        }

    }

    public void measureTime(int version) throws InterruptedException {
        if (version == 7) {
            long start = System.currentTimeMillis();
            Thread.sleep(1000);
            long end = System.currentTimeMillis();
            System.out.println(end - start);

        }
        else if (version == 8) {
            Instant start = Instant.now();
            Thread.sleep(1000);
            Instant end = Instant.now();
            System.out.println(Duration.between(start, end));

        }
    }

    void betweenDates (int version) {
        if (version == 7) {
            Calendar birthdate = Calendar.getInstance();
            Calendar current = Calendar.getInstance();

            birthdate.set(2000, 8, 14);
            current.set(2015, 4, 21);

            int years = 0;

            while (birthdate.before(current)) {
                birthdate.add(Calendar.YEAR, 1);
                if (birthdate.before(current)) {
                    years++;
                }
            }
            System.out.println(years);
        }
        else if (version == 8) {
            LocalDate birthdate = LocalDate.of(2000, 8, 14);
            LocalDate current = LocalDate.now();

            Period period = Period.between(birthdate, current);
            System.out.println(period.getYears() + " years with " + period.getMonths()
                    + " months with " + period.getYears() + " days have passed since " +
                    birthdate + " to " + current);
        }

    }

    public void cast(int version) throws ParseException {
        if (version == 7){
            String date = "14/08/2000";
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date dateCasted = formatter.parse(date);
            System.out.println(dateCasted);

            Date currentDate = Calendar.getInstance().getTime();
            formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
            String stringCasted = formatter.format(currentDate);
            System.out.println(stringCasted);
        } else if (version == 8) {
            String date = "14/08/2000";
            DateTimeFormatter  formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate localDate = LocalDate.parse(date, formatter);
            System.out.println(localDate);

            formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
            System.out.println(formatter.format(localDate));
        }
    }

    public static void main(String[] args) {
        DateApp app = new DateApp();

        try{
            app.verify(8);
            app.measureTime(8);
            app.betweenDates(8);
            app.cast(8);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}
