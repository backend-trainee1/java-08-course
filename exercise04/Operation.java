package exercise04;

@FunctionalInterface
public interface Operation {
    void salute();
}
