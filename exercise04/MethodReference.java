package exercise04;

import java.util.Arrays;

public class MethodReference {

    public static void staticReference(){
        System.out.println("Static Method Reference");
    }

    public void objectReference(){
        String[] names = {"Daniela", "Paola", "Diego", "Orlando"};

        Arrays.sort(names, String::compareToIgnoreCase);
        System.out.println(Arrays.toString(names));
    }

    public void particularObjectReference(){
        System.out.println("Particular Object Method Reference");
    }

    public void constructorReference(){
        IPerson personInterface = Person::new;
        Person person = personInterface.create(1, "Daniela");
        System.out.printf("ID: %d\nName: %s", person.getId(), person.getName());
    }

    public void operate(){
        Operation operation = MethodReference::staticReference;
        operation.salute();
    }


    public static void main(String[] args) {
        MethodReference app = new MethodReference();
        //app.operate();
        //app.objectReference();
        //Operation op = app::particularObjectReference;
        //op.salute();
        app.constructorReference();
    }
}
