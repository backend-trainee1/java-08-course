package exercise04;

@FunctionalInterface
public interface IPerson {
    Person create(int id, String name);
}
