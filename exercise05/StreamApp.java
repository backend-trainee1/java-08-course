package exercise05;

import java.util.ArrayList;
import java.util.List;

public class StreamApp {
    private List<String> list;
    private List<String> numbers;

    public StreamApp(){
        list = new ArrayList<>();
        list.add("Programming");
        list.add("Maths I");
        list.add("Maths II");
        list.add("Statistics");

        numbers = new ArrayList<>();
        numbers.add("1");
        numbers.add("2");
        numbers.add("3");
    }

    public void filterFunc(){
        list.stream().filter(x -> x.startsWith("M")).forEach(System.out::println);
    }

    public void sortFunc(){
        list.stream().sorted(String::compareTo).forEach(System.out::println);
    }

    public void mapFunc(){
        numbers.stream().map(x->Integer.parseInt(x) + 1).forEach(System.out::println);
    }

    public void limitFunc(){
        list.stream().limit(2).forEach(System.out::println);
    }

    public void countFunc(){
        long x = list.stream().count();
        System.out.println(x);
    }

    public static void main(String[] args) {
        StreamApp app = new StreamApp();
        app.filterFunc();
        app.sortFunc();
        app.mapFunc();
        app.limitFunc();
        app.countFunc();
    }
}
