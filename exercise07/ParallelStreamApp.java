package exercise07;

import java.util.ArrayList;
import java.util.List;

public class ParallelStreamApp {
    private List<Integer> numbers;

    public void fillUp(){
        numbers = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            numbers.add(i);
        }
    }

    public void tryStream(){
        numbers.stream().forEach(System.out::println);
    }

    public void tryParallel(){
        numbers.parallelStream().forEach(System.out::println);
    }

    public static void main(String[] args) {
        ParallelStreamApp app = new ParallelStreamApp();

        app.fillUp();
        app.tryParallel();
    }
}
